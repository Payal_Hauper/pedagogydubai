import Foundation

public enum EnumLoginType {
    case yellowBrickRoadNursery
    case crystalValleyBrickRoadNursery
}

enum SideMenuOption : Int {
    case home = 0
    case event
    case sponsor
}

