//
//  MilestoneTableViewCell.swift
//  PedagogyDubai
//
//  Created by === on 23/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit

class MilestoneTableViewCell: UITableViewCell {

    @IBOutlet weak var viewComments: UIView!
    @IBOutlet weak var lblComments: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.viewComments.layer.cornerRadius = 10
    }
    
    func setUp(data: FeedDetailsDM) {
        self.lblComments.text = data.comment
        self.lblComments.sizeToFit()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
