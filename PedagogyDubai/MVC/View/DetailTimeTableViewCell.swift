//
//  DetailTimeTableViewCell.swift
//  PedagogyDubai
//
//  Created by === on 23/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit

class DetailTimeTableViewCell: UITableViewCell {

    @IBOutlet weak var lblTimeAgo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblTimeAgo.sizeToFit()
    }
    
    func setUp(timeAgoString: String) {
        lblTimeAgo.text = timeAgoString
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
