//
//  CustomCellableViewFeedDetailsMileStone.swift
//  PedagogyDubai
//
//  Created by === on 23/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit

class CustomCellableViewFeedDetailsMileStone: UITableViewCell {

    @IBOutlet weak var viewPoint: UIView!
    @IBOutlet weak var lblMileStoneMessage: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblMileStoneMessage.sizeToFit()
//        self.viewPoint.layoutIfNeeded()
        self.viewPoint.layer.cornerRadius = self.viewPoint.frame.height / 2
    }
    
    func setUp(milestoneString: String) {
        lblMileStoneMessage.text = milestoneString
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
