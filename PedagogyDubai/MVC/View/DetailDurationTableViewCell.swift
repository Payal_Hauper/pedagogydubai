//
//  DetailDurationTableViewCell.swift
//  PedagogyDubai
//
//  Created by === on 23/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit

class DetailDurationTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDuration: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUp(dateString: String) {
        self.lblDuration.text = dateString
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
