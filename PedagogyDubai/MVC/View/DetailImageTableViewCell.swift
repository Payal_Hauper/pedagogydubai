//
//  DetailImageTableViewCell.swift
//  PedagogyDubai
//
//  Created by === on 23/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit
import SDWebImage

class DetailImageTableViewCell: UITableViewCell {

    @IBOutlet weak var imgViewPlay: UIImageView!
    @IBOutlet weak var imgview: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setUp(urlString: String, isShowPlayBtn: Bool = false) {
        self.imgViewPlay.isHidden = !isShowPlayBtn

        if isShowPlayBtn {
            self.imgview.backgroundColor = UIColor.darkGray
            self.imgview.image = nil
        }
        else {
            let solidImage = UIColor.darkGray.image(CGSize(width: self.imgview.frame.width, height: self.imgview.frame.width))
            self.imgview.sd_setImage(with: URL(string: urlString), placeholderImage: solidImage)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
