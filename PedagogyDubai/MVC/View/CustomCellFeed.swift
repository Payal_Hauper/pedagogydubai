import UIKit

class CustomCellFeed: UICollectionViewCell {
    
    @IBOutlet weak var imgViewIcon: UIImageView!
    @IBOutlet weak var imgViewPlay: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var data : FeedDM! {
        didSet {
            self.lblTitle.text = data.title
            self.lblDescription.text = data.time_ago

            if data.pdf.contains(".pdf") {
                let pdfImage = UIImage(named: "pdf")
                self.imgViewIcon.image = pdfImage
                self.imgViewPlay.isHidden = true
                self.imgViewIcon.backgroundColor = UIColor.clear
            } else if data.image.contains(".mp4") {
                self.imgViewIcon.backgroundColor = UIColor.lightGray
                self.imgViewIcon.image = nil
                self.imgViewPlay.isHidden = false
            } else if data.type.contains("4") {
                let pdfImage = UIImage(named: "bullet")
                self.imgViewIcon.image = pdfImage
                self.imgViewPlay.isHidden = true
            } else {
                self.imgViewPlay.isHidden = true
                self.imgViewIcon.backgroundColor = UIColor.clear
                let solidImage = UIColor.darkGray.image(CGSize(width: self.imgViewIcon.frame.width, height: self.imgViewIcon.frame.width))
                self.imgViewIcon.sd_setImage(with: URL(string: data.image), placeholderImage: solidImage)
            }
        }
    }
    
}
