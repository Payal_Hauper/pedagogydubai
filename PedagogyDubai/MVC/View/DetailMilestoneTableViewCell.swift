//
//  DetailMilestoneTableViewCell.swift
//  PedagogyDubai
//
//  Created by sahdevsinh chavda on 21/09/19.
//  Copyright © 2019 sahdevsinh chavda. All rights reserved.
//

import UIKit

class DetailMilestoneTableViewCell: UITableViewCell {

    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var lblSubCategory: UILabel!
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var viewPoint: UIView!
    @IBOutlet weak var viewUnderline: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.viewPoint.layer.cornerRadius = self.viewPoint.frame.height / 2
    }
    
    func setUp(data: MilestonePointsDM) {
        lblCategory.text = data.category
        lblSubCategory.text = data.subCategory
        lblDetails.text = data.details
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
