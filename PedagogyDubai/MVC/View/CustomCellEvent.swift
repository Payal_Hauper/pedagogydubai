//
//  CustomCellEvent.swift
//  PedagogyDubai
//
//  Created by === on 15/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit

class CustomCellEvent: UICollectionViewCell {
    @IBOutlet weak var imgViewIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblEventDuration: UILabel!
    @IBOutlet weak var lblTimeAgo: UILabel!
    
    var data : EventDM! {
        didSet {
            self.lblTitle.text = data.name
            self.lblTimeAgo.text = data.time_ago
            let solidImage = UIColor.darkGray.image(CGSize(width: self.imgViewIcon.frame.width, height: self.imgViewIcon.frame.width))
            self.imgViewIcon.sd_setImage(with: URL(string: data.image), placeholderImage: solidImage)
            self.lblEventDuration.text = "Date: \(data.start_date) - \(data.end_date)"
        }
    }
}
