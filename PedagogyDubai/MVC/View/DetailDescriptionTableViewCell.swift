//
//  DetailDescriptionTableViewCell.swift
//  PedagogyDubai
//
//  Created by === on 23/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit

class DetailDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDescription: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.lblDescription.sizeToFit()
    }

    func setUp(descString: String) {
        self.lblDescription.text = descString
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
