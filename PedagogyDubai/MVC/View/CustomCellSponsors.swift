//
//  CustomCellSponsors.swift
//  PedagogyDubai
//
//  Created by === on 16/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit

class CustomCellSponsors: UICollectionViewCell {
    @IBOutlet weak var imgViewBG: UIImageView!
    @IBOutlet weak var imgViewIcon: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var currentIndex : Int! {
        didSet {
            let imgIndex = (currentIndex % 9)
            print("Image Name :- bgImage_sponsor\(imgIndex)")
            self.imgViewBG.image = UIImage(named: "bgImage_sponsor\(imgIndex)")
        }
    }
    
    var data : SponsorDM! {
        didSet {
            self.lblTitle.text = data.description
            let solidImage = UIColor.clear.image(CGSize(width: self.imgViewIcon.frame.width, height: self.imgViewIcon.frame.width))
            self.imgViewIcon.sd_setImage(with: URL(string: data.logo), placeholderImage: solidImage)
        }
    }


}
