//
//  SponsorDM.swift
//  PedagogyDubai
//
//  Created by === on 16/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import Foundation
import SwiftyJSON

class SponsorDM: BaseDM {
    
    
    var id = ""
    var title = ""
    var description = ""
    var logo = ""
    var link = ""
    var time_ago = ""
    
    
    required init() {
        super.init()
    }
    
    required init(jsonData : JSON) {
        if jsonData["status"].bool != nil {
            super.init(jsonData: jsonData)
        }
        else {
            super.init()
        }

        let eventData = jsonData
        
        id = eventData["id"].stringValue
        title = eventData["title"].stringValue
        description = eventData["description"].stringValue
        logo = eventData["logo"].stringValue
        link = eventData["link"].stringValue
        time_ago = eventData["time_ago"].stringValue
    }
    
    class func getSponsorDM(arrJson : [JSON]) -> [SponsorDM] {
        return arrJson.compactMap({ (objJson) -> SponsorDM in
            return SponsorDM.init(jsonData: objJson)
        })
    }

}
