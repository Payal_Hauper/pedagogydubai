//
//  EventDM.swift
//  PedagogyDubai
//
//  Created by === on 15/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import Foundation
import SwiftyJSON

class EventDM : BaseDM {
    
    var id  = ""
    var name = ""
    var start_date = ""
    var end_date = ""
    var terms_of_month = ""
    var image = ""
    var time_ago = ""
    var description = ""
    
    required init() {
        super.init()
    }

    required init(jsonData: JSON) {
        if jsonData["status"].bool != nil {
            super.init(jsonData: jsonData)
        }
        else {
            super.init()
        }
        
        let eventData = jsonData
        
        id = eventData["id"].stringValue
        name = eventData["name"].stringValue
        start_date = eventData["start_date"].stringValue
        end_date = eventData["end_date"].stringValue
        terms_of_month = eventData["terms_of_month"].stringValue
        image = eventData["image"].stringValue
        time_ago = eventData["time_ago"].stringValue
        description = eventData["description"].stringValue
    }
    
    class func getEventDM(arrJson : [JSON]) -> [EventDM] {        
        return arrJson.compactMap { (objJson) -> EventDM in
            return EventDM.init(jsonData: objJson)
        }
    }
}

