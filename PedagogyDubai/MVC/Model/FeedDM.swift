import Foundation
import SwiftyJSON


class FeedDM : BaseDM {
    
    var id : String = ""
    var title : String = ""
    var description : String = ""
    var type : String = ""
    var image : String = ""
    var time_ago : String = ""
    var pdf = ""
    
    required init() {
        super.init()
    }

    required init(jsonData : JSON) {
        if jsonData["status"].bool != nil {
            super.init(jsonData: jsonData)
        }
        else {
            super.init()
        }
        let feedData = jsonData
        id = feedData["id"].stringValue
        title = feedData["title"].stringValue
        description = feedData["description"].stringValue
        type = feedData["type"].stringValue
        image = feedData["image"].stringValue
        time_ago = feedData["time_ago"].stringValue
        pdf = feedData["pdf"].stringValue
    }

    class func getFeedDM(arrJson : [JSON]) -> [FeedDM] {
        return arrJson.compactMap({ (objJosn) -> FeedDM in
            return FeedDM.init(jsonData: objJosn)
        })
    }
}
