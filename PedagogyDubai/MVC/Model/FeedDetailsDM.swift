import UIKit

class FeedDetailsDM {
    
    var id : String = ""
    var title : String = ""
    var description : String = ""
    var type : String = ""
    var assets : [String] = []
    var time_ago : String = ""
    var milestone_points : [MilestonePointsDM] = []
    var comment : String = ""
    var detail_ids : String = ""
    
    init() {
    }
    
    init(jsonData : Dictionary<String,Any>) {
        
        id = jsonData["id"] as? String ?? ""
        title = jsonData["title"] as? String ?? ""
        description = jsonData["description"] as? String ?? ""
        type = jsonData["type"] as? String ?? ""
        assets = jsonData["assets"] as? [String] ?? []
        time_ago = jsonData["time_ago"] as? String ?? ""
        
        milestone_points = MilestonePointsDM.milestoneArray(jsonData["milestone_points"] as Any)
        comment = jsonData["comment"] as? String ?? ""
        detail_ids = jsonData["detail_ids"] as? String ?? ""

    }
    
    class func feedDetailsData(jsonArray : [Dictionary<String,Any>]) -> [FeedDetailsDM] {
        
        var arrFeedDetailsData = [FeedDetailsDM]()
        
        for json in jsonArray {
            let obj = FeedDetailsDM.init(jsonData: json)
            arrFeedDetailsData.append(obj)
        }
        return arrFeedDetailsData
    }
    
}


struct MilestonePointsDM {
    var category : String
    var subCategory : String
    var details : String
    
    init(jsonData : AnyObject) {
        category = jsonData["category"] as? String ?? ""
        subCategory = jsonData["subcategory"] as? String ?? ""
        details = jsonData["details"] as? String ?? ""
    }
    
    static func milestoneArray(_ milestone: Any?) -> [MilestonePointsDM] {
        guard let milestoneArray = milestone as? [AnyObject]  else {
            return []
        }
        return (milestoneArray).compactMap { (obj) -> MilestonePointsDM in
            return MilestonePointsDM(jsonData: obj)
        }
    }
    
}
