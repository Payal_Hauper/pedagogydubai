//
//  BaseDM.swift
//  PedagogyDubai
//
//  Created by === on 15/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import Foundation
import SwiftyJSON

class BaseDM {
    
    static var total_posts = 0
    var status = ""
    var message = ""
    var data = [JSON]()
    
    required init() {}
    
    required init(jsonData: JSON) {
        BaseDM.total_posts = jsonData["total_posts"].intValue
        status = jsonData["status"].stringValue
        message = jsonData["message"].stringValue
        data = jsonData["data"].arrayValue
    }

}
