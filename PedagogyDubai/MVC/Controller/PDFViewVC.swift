import UIKit
import SDWebImagePDFCoder

class PDFViewVC: UIViewController {

    @IBOutlet weak var imgViewPDF: UIImageView!
    var strPDFURL = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let PDFCoder = SDImagePDFCoder.shared
        SDImageCodersManager.shared.addCoder(PDFCoder)
        let PDFImageSize = imgViewPDF.bounds.size
        imgViewPDF.sd_setImage(with: URL(string: strPDFURL), placeholderImage: nil, options: [], context: [.pdfImageSize : PDFImageSize])
    }
    
}
