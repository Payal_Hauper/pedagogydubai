
import UIKit
import SideMenu
import SwiftyJSON
import Alamofire
import SDWebImage
import SDWebImagePDFCoder

class HomeVC: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var viewBG: UIView!
    @IBOutlet weak var btnMail: UIButton!
    @IBOutlet weak var collectionViewFeed: UICollectionView!
    @IBOutlet weak var lblNoDataFound: UILabel!
    
    let btnSideMenu = UIButton()
    let btnSearch = UIButton()
    let txtSearch = UITextField()
    let lblTitle =  UILabel()
    
    var refreshControl = UIRefreshControl()
    
    var viewMoreOption = UIView()
    
    var arrMoreOption = ["Settings"]
    var arrFeedDetails = [FeedDM]()
    var arrEventDetails = [EventDM]()
    var arrSponsorsDetails = [SponsorDM]()
    
    var arrDefaultFeedDetails = [FeedDM]()
    var arrDefaultEventDetails = [EventDM]()
    var arrDefaultSponsorsDetails = [SponsorDM]()

    var activityView : ActivityView!
    
    var hideFooter: Bool = true
    
    var page = 1
    
    var sideMenu : SideMenuContainerVC?


    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.activityView.activityView.stopAnimating()
        self.activityView.removeFromSuperview()
    }
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        
        print(sender)
    }
    
    //MARK:- Methods
    func setup() {
        
        self.view.layoutIfNeeded()
        self.setupNavigationBar()
        self.setupSideMenu()

        self.btnMail.layer.cornerRadius = self.btnMail.frame.width / 2
        self.btnMail.layoutIfNeeded()
        self.btnMail.isHidden = true
        
        self.lblNoDataFound.isHidden = true
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
        collectionViewFeed.addSubview(refreshControl)
        self.collectionViewFeed.alwaysBounceVertical = true
        
        self.page = 1
        self.setupAndCallApi(searchText: self.txtSearch.text!)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.notificationOFChangeTab(notification:)), name: Notification.Name("notificationOFChangeTab"), object: nil)
    }
    
    @objc func notificationOFChangeTab(notification: Notification) {
        
        print(notification)
        let tab = ((notification.userInfo as! [String:Any])["selectedTab"]! as! SideMenuOption)
        if !(AppSingleton.sharedInstance().selectedSideMenu == tab) {
            
            AppSingleton.sharedInstance().selectedSideMenu = tab
            
            self.arrFeedDetails.removeAll()
            self.arrEventDetails.removeAll()
            self.arrSponsorsDetails.removeAll()
            self.collectionViewFeed.reloadData()

            switch AppSingleton.sharedInstance().selectedSideMenu {
            case .home:
                self.lblTitle.text = "Home"
            case .event:
                self.lblTitle.text = "Event"
            case .sponsor:
                self.lblTitle.text = "Parent Community Connections"
            }
            
            self.page = 1
            self.setupAndCallApi(searchText: self.txtSearch.text!)
        }
    }


    @objc func refresh(sender:AnyObject) {
        self.page = 1
        self.arrFeedDetails.removeAll()
        self.arrEventDetails.removeAll()
        self.arrSponsorsDetails.removeAll()
        self.setupAndCallApi(searchText: self.txtSearch.text!)
        self.refreshControl.endRefreshing()
    }
    
    func setupSideMenu() {
        
        SideMenuManager.default.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        // Set up a cool background image for demo purposes
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor.black
        SideMenuManager.default.menuWidth = view.frame.width * 0.7
    }

    func setupNavigationBar() {
        
        let width = self.view.frame.width - 60
        let viewLeftbar = UIView(frame: CGRect.init(x: 0, y: 0, width: width, height: 40))
        viewLeftbar.backgroundColor = .clear
        
        //let btnSideMenu = UIButton(type: .custom)
        btnSideMenu.setImage(UIImage(named: "sldeMenu"), for: .normal)
        btnSideMenu.frame = CGRect(x: 0, y: 0, width: 25, height: 40)
        btnSideMenu.addTarget(self, action: #selector(btnSideMenuClicked), for: .touchUpInside)

        lblTitle.frame = CGRect(x: 40, y: 0, width: width - 40, height: 40)
        lblTitle.text = "Home"
        lblTitle.textColor = UIColor.white
        lblTitle.font = UIFont.boldSystemFont(ofSize: 17.0) //UIFont(name: "Merienda-Bold", size: 17.0)
        
        txtSearch.frame = CGRect(x: 40, y: 0, width: width - 40, height: 40)
        txtSearch.delegate = self
        txtSearch.placeholder = "Search"
        txtSearch.isHidden = true
        btnSearch.tag = 0
        txtSearch.attributedPlaceholder = NSAttributedString(string: "Search...",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        viewLeftbar.addSubview(btnSideMenu)
        viewLeftbar.addSubview(lblTitle)
        viewLeftbar.addSubview(txtSearch)

        let leftBarItem = UIBarButtonItem(customView: viewLeftbar)

        btnSearch.setImage(UIImage(named: "search"), for: .normal)
        btnSearch.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        btnSearch.addTarget(self, action: #selector(btnSearchClicked), for: .touchUpInside)
        let rightBarItem = UIBarButtonItem(customView: btnSearch)
        
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
        self.navigationItem.setRightBarButton(rightBarItem, animated: true)
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        
        self.navigationController?.navigationBar.barStyle = .black
        self.navigationController?.navigationBar.isHidden  = false
    }
    
    var sideMenuNav : UISideMenuNavigationController?
    @objc func btnSideMenuClicked() {
    
        if self.btnSearch.tag == 0 {
            // open side menu
            sideMenuNav = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as! UISideMenuNavigationController
            self.sideMenu = sideMenuNav!.children.first! as? SideMenuContainerVC
            self.sideMenu?.delegateSideMenu = self
            present(sideMenuNav!, animated: true, completion: nil)
        } else {
            //cancel search
            self.lblTitle.isHidden = false
            self.txtSearch.isHidden = true
            self.btnSearch.isHidden = false
            self.btnSearch.tag = 0
            DispatchQueue.main.async {
                self.btnSideMenu.setImage(UIImage(named: "sldeMenu"), for: .normal)
                self.btnSearch.setImage(UIImage(named: "search"), for: .normal)
                self.btnSearch.setImage(UIImage(named: "search"), for: .selected)

            }
            
            self.arrFeedDetails.removeAll()
            self.arrEventDetails.removeAll()
            self.arrSponsorsDetails.removeAll()
            self.arrDefaultEventDetails.removeAll()
            self.arrDefaultEventDetails.removeAll()
            self.arrDefaultSponsorsDetails.removeAll()
            self.collectionViewFeed.reloadData()

            
            self.txtSearch.resignFirstResponder()
            self.page = 1
            
           // if self.txtSearch.text!.count > 0 {
                self.setupAndCallApi(searchText: "")
          //  }
            self.txtSearch.text = ""
        }
    }
    
    @objc func btnSearchClicked() {

        if self.btnSearch.tag == 0 {
            //search button
            self.lblTitle.isHidden = true
            self.txtSearch.isHidden = false
            self.btnSearch.isHidden = true
            self.btnSearch.tag = 1
            self.txtSearch.becomeFirstResponder()
            self.txtSearch.textColor = UIColor.white
            self.btnSideMenu.setImage(UIImage(named: "back"), for: .normal)
        } else {
            //clear in searchbar text
            self.txtSearch.text = ""
            self.btnSearch.isHidden = true
            self.collectionViewFeed.reloadData()
            self.lblNoDataFound.isHidden = true
            self.collectionViewFeed.isHidden = false
            self.arrFeedDetails.removeAll()
            self.arrEventDetails.removeAll()
            self.arrSponsorsDetails.removeAll()
            self.arrDefaultEventDetails.removeAll()
            self.arrDefaultEventDetails.removeAll()
            self.arrDefaultSponsorsDetails.removeAll()
            
            self.collectionViewFeed.reloadData()
            
            self.page = 1
            self.setupAndCallApi(searchText: "")
        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.viewMoreOption.removeFromSuperview()
        
        for recognizer in view.gestureRecognizers ?? [] {
            if (recognizer is UITapGestureRecognizer) {
                self.view.removeGestureRecognizer(recognizer)
            }
        }
    }

    //MARK:- Dashboard Api
    
    func setupAndCallApi(searchText : String)  {
        
        if !Connectivity.isConnectedToInternet {
            self.lblNoDataFound.text = "No Inernet connection"
            self.collectionViewFeed.isHidden = true
            self.lblNoDataFound.isHidden = false
            return
        }
        
        self.lblNoDataFound.isHidden = true
        self.arrFeedDetails.removeAll()
        self.collectionViewFeed.reloadData()
        
        switch AppSingleton.sharedInstance().selectedSideMenu {
        case .home:
            self.callDashboardApi(searchText: searchText, page: self.page)
        case .event:
            self.callEventApi(searchText: searchText, page: self.page)
        case .sponsor:
            self.callSponsorApi(searchText: searchText, page: self.page)
        }
    }
    
    //,selectedTab: SideMenuOption
    func callDashboardApi(searchText : String,page: Int) {
        
        if page == 1 && searchText.count == 0 {
            self.setupActivityView()
        }
        
        let strURL = ApiDetails.dashboard.url + "\(UserDefaults.standard.value(forKey: "UserID") ?? 0)/feed/" + "?search=\(searchText)&page=\(page)"
        
        let urlString = strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)

        apiManager.apiCalling(urlString: urlString!, method: ApiDetails.dashboard.method, param: nil, header: ApiDetails.dashboard.header, encodingTye: JSONEncoding.default) { (code, response, error) in
            
            self.hideFooter = true
            
            if page == 1 && searchText.count == 0 {
                   self.activityView.activityView.stopAnimating()
                   self.activityView.removeFromSuperview()
            }
            
            guard error == nil else {
                if (error as! NSError).code != -999 {
                  //  AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: error!.localizedDescription, actionButtonTitle: "Ok")
                    
                    self.arrFeedDetails = self.arrDefaultFeedDetails
                    
                    if self.arrFeedDetails.count > 0 {
                        self.lblNoDataFound.isHidden = true
                        self.collectionViewFeed.isHidden = false
                        self.collectionViewFeed.reloadData()
                    } else {
                        self.lblNoDataFound.isHidden = false
                        self.lblNoDataFound.text = "No Data Found."
                        self.collectionViewFeed.isHidden = true
                    }
                }
                return
            }

            let arrFeedData = FeedDM.init(jsonData: response)
            
            self.arrFeedDetails.append(contentsOf: FeedDM.getFeedDM(arrJson: arrFeedData.data))
            
            print(self.arrEventDetails)
            
            if self.arrFeedDetails.count > 0 {
                if self.arrDefaultFeedDetails.count == 0 {
                    self.arrDefaultFeedDetails = self.arrFeedDetails
                }
                self.lblNoDataFound.isHidden = true
                self.collectionViewFeed.isHidden = false
                self.collectionViewFeed.reloadData()
            } else {
                self.lblNoDataFound.isHidden = false
                self.lblNoDataFound.text = "No Data Found."
                self.collectionViewFeed.isHidden = true
            }
        }
    }
    
    func callEventApi(searchText : String,page: Int) {
        
        if page == 1 && searchText.count == 0 {
            self.setupActivityView()
        }
        let strURL = ApiDetails.event.url + "?page=\(page)&search=\(searchText)"
        let urlString = strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        apiManager.apiCalling(urlString: urlString!, method: ApiDetails.event.method, param: nil, header: ApiDetails.event.header, encodingTye: JSONEncoding.default) { (code, response, error) in
            
            self.hideFooter = true
            
            if page == 1 && searchText.count == 0 {
                self.activityView.activityView.stopAnimating()
                self.activityView.removeFromSuperview()
            }
            
            guard error == nil else {
                if (error as! NSError).code != -999 {
                   // AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: error!.localizedDescription, actionButtonTitle: "Ok")
                    self.arrEventDetails = self.arrDefaultEventDetails
                    if self.arrEventDetails.count > 0 {
                        self.lblNoDataFound.isHidden = true
                        self.collectionViewFeed.isHidden = false
                        self.collectionViewFeed.reloadData()
                    } else {
                        self.lblNoDataFound.isHidden = false
                        self.lblNoDataFound.text = "No Data Found."
                        self.collectionViewFeed.isHidden = true
                    }
                }
                return
            }
            
            let arrEventData = EventDM.init(jsonData: response)
            
            self.arrEventDetails.append(contentsOf: EventDM.getEventDM(arrJson: arrEventData.data))
            print(self.arrEventDetails)
            
            if self.arrEventDetails.count > 0 {
                
                if self.arrDefaultEventDetails.count == 0 {
                    self.arrDefaultEventDetails = self.arrEventDetails
                }
                self.lblNoDataFound.isHidden = true
                self.collectionViewFeed.isHidden = false
                self.collectionViewFeed.reloadData()
            } else {
                self.lblNoDataFound.isHidden = false
                self.lblNoDataFound.text = "No Data Found."
                self.collectionViewFeed.isHidden = true
            }
        }
    }

    func callSponsorApi(searchText : String,page: Int) {
        
        if page == 1 && searchText.count == 0 {
            self.setupActivityView()
        }
        
        let strURL = ApiDetails.sponsor.url + "?search=\(searchText)&page=\(page)"
        
        let urlString = strURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        apiManager.apiCalling(urlString: urlString!, method: ApiDetails.sponsor.method, param: nil, header: ApiDetails.sponsor.header, encodingTye: JSONEncoding.default) { (code, response, error) in
            
            self.hideFooter = true
            
            if page == 1 && searchText.count == 0 {
                self.activityView.activityView.stopAnimating()
                self.activityView.removeFromSuperview()
            }
            
            guard error == nil else {
                if (error as! NSError).code != -999 {
                    //AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: error!.localizedDescription, actionButtonTitle: "Ok")
                    self.arrSponsorsDetails = self.arrDefaultSponsorsDetails
                    
                    if self.arrSponsorsDetails.count > 0 {
                        self.lblNoDataFound.isHidden = true
                        self.collectionViewFeed.isHidden = false
                        self.collectionViewFeed.reloadData()
                    } else {
                        self.lblNoDataFound.isHidden = false
                        self.lblNoDataFound.text = "No Data Found."
                        self.collectionViewFeed.isHidden = true
                    }
                }
                return
            }
            
            let arrSponsorData = SponsorDM.init(jsonData: response)
            
            self.arrSponsorsDetails.append(contentsOf: SponsorDM.getSponsorDM(arrJson: arrSponsorData.data))
            print(self.arrSponsorsDetails)
            
            if self.arrSponsorsDetails.count > 0 {
                
                if self.arrDefaultSponsorsDetails.count == 0 {
                    self.arrDefaultSponsorsDetails = self.arrSponsorsDetails
                }

                self.lblNoDataFound.isHidden = true
                self.collectionViewFeed.isHidden = false
                self.collectionViewFeed.reloadData()
            } else {
                self.lblNoDataFound.isHidden = false
                self.lblNoDataFound.text = "No Data Found."
                self.collectionViewFeed.isHidden = true
            }
        }
    }
    
    func setupActivityView() {
        activityView = (Bundle.main.loadNibNamed("ActivityView", owner: nil, options: nil)![0] as! ActivityView)
        activityView.activityView.startAnimating()
        self.activityView.frame = self.view.frame
        self.activityView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(self.activityView)
    }
    
    func pushToFeedDetailsScreen(id : Int,type : Int?,eventDetails : EventDM?) {
        let feedDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "FeedDetailsVC") as! FeedDetailsVC
        feedDetailsVC.currentIndex = id
        feedDetailsVC.apiType = type
        feedDetailsVC.eventDetails = eventDetails
        self.navigationController?.pushViewController(feedDetailsVC, animated: true)
    }
    
    func pushToPDFScreen(url : URL, title: String) {
        let newPDF = self.storyboard?.instantiateViewController(withIdentifier: "NewPDFVC") as! NewPDFVC
        newPDF.url = url
        newPDF.name = title
        self.navigationController?.pushViewController(newPDF, animated: true)
    }
    
    func checkForPaginationApi(currentIndex:Int,arrTotalCount: Int,searchText : String = "") {
        
        if arrTotalCount - 1 == currentIndex {
            self.page = page + 1
            //self.callDashboardApi(searchText: "", page: self.page)
            
            switch AppSingleton.sharedInstance().selectedSideMenu {
            case .home:
                self.callDashboardApi(searchText: searchText, page: self.page)
            case .event:
                self.callEventApi(searchText: searchText, page: self.page)
            case .sponsor:
                self.callSponsorApi(searchText: searchText, page: self.page)
            }
            
            self.hideFooter = false
        } else {
            self.hideFooter = true
        }
    }

    //MARK:- Action
    @IBAction func btnMailClicked(_ sender: UIButton) {
        
    }
    
    var total_posts: Int = 0
}

extension HomeVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch AppSingleton.sharedInstance().selectedSideMenu {
        case .home:
            return self.arrFeedDetails.count
        case .event:
            return self.arrEventDetails.count
        case .sponsor:
            return self.arrSponsorsDetails.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch AppSingleton.sharedInstance().selectedSideMenu {
        case .home:
            let cell = self.collectionViewFeed.dequeueReusableCell(withReuseIdentifier: "CustomCellFeed", for: indexPath) as! CustomCellFeed
            cell.data = self.arrFeedDetails[indexPath.item]
            return cell
        case .event:
            let cell = self.collectionViewFeed.dequeueReusableCell(withReuseIdentifier: "CustomCellEvent", for: indexPath) as! CustomCellEvent
            cell.data = self.arrEventDetails[indexPath.item]
            return cell
        case .sponsor:
            let cell = self.collectionViewFeed.dequeueReusableCell(withReuseIdentifier: "CustomCellSponsors", for: indexPath) as! CustomCellSponsors
            cell.currentIndex = indexPath.item
            cell.data = self.arrSponsorsDetails[indexPath.item]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch AppSingleton.sharedInstance().selectedSideMenu {
        case .home:
            let data = self.arrFeedDetails[indexPath.item]
            if data.pdf.contains(".pdf") {
                guard let url = URL(string: data.pdf) else { return }
                self.pushToPDFScreen(url: url, title: data.title)
            } else {
                self.pushToFeedDetailsScreen(id: Int(data.id)!, type: Int(data.type)!, eventDetails: nil)
            }
        case .event:
            print("sponsor clicked")
            let data = self.arrEventDetails[indexPath.item]
            self.pushToFeedDetailsScreen(id: Int(data.id)!, type: nil, eventDetails: data)
        case .sponsor:
            let data = self.arrSponsorsDetails[indexPath.item]
            guard let url = URL(string: data.link) else { return }
            UIApplication.shared.open(url)
            print("sponsor clicked")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch AppSingleton.sharedInstance().selectedSideMenu {
        case .home:
            return CGSize(width: self.collectionViewFeed.bounds.width, height: 100)
        case .event:
            return CGSize(width: self.collectionViewFeed.bounds.width, height: 100)
        case .sponsor:
            return CGSize(width: self.collectionViewFeed.bounds.width, height: 100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        switch AppSingleton.sharedInstance().selectedSideMenu {
        case .home:
            if self.arrFeedDetails.count < BaseDM.total_posts {
                self.checkForPaginationApi(currentIndex: indexPath.item, arrTotalCount: self.arrFeedDetails.count,searchText: self.txtSearch.text!)
            } else {
                self.hideFooter = true
            }
        case .event:
            if self.arrEventDetails.count < BaseDM.total_posts {
                self.checkForPaginationApi(currentIndex: indexPath.item, arrTotalCount: self.arrEventDetails.count,searchText: self.txtSearch.text!)
            } else {
                self.hideFooter = true
            }
        case .sponsor:
            if self.arrSponsorsDetails.count < BaseDM.total_posts {
                self.checkForPaginationApi(currentIndex: indexPath.item, arrTotalCount: self.arrSponsorsDetails.count,searchText: self.txtSearch.text!)
            }else {
                self.hideFooter = true
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        
        if self.hideFooter {
            return CGSize.zero
        } else {
            return CGSize(width: self.collectionViewFeed.bounds.width, height: 50)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {
            
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "collectionFeedFooterView", for: indexPath)
            headerView.backgroundColor = UIColor.clear
            return headerView
        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "collectionFeedFooterView", for: indexPath) as! collectionFeedFooterView
            footerView.backgroundColor = UIColor.clear
            footerView.activityView.startAnimating()
            return footerView
        default:
            assert(false, "Unexpected element kind")
            return UIView(frame: CGRect.zero) as! UICollectionReusableView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if AppSingleton.sharedInstance().selectedSideMenu == .sponsor {
            return 8
        }
        return 0
    }
    
}


extension HomeVC : UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true;
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let updatedString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if updatedString.count > 0 {
            self.btnSearch.isHidden = false
        }
        self.btnSearch.setImage(UIImage(named: "cancel"), for: .normal)
        
        self.arrFeedDetails.removeAll()
        self.arrEventDetails.removeAll()
        self.arrSponsorsDetails.removeAll()
        
        self.page = 1
        self.setupAndCallApi(searchText: updatedString)
        print(updatedString)
        return true;
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
}

extension HomeVC : ProtocolDismissSideMenu {
    func dismissSideMenu() {
        sideMenuNav!.dismiss(animated: true, completion: nil)
        
        AppSingleton.sharedInstance().selectedSideMenu = .home
        
        UserDefaults.standard.removeObject(forKey: "UserDetails")
        UserDefaults.standard.set(false, forKey: "UserLoginStatus")
        UserDefaults.standard.set("", forKey: "UserID")
        let logincVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ViewController") as? ViewController
        let navController = UINavigationController(rootViewController: logincVC!)
        appDel.window?.rootViewController = navController
    }
}
