//
//  NewPDFVC.swift
//  PedagogyDubai
//
//  Created by Gaurav on 22/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit
import PDFKit

class NewPDFVC: UIViewController {

    var url : URL?
    var lblTitle = UILabel()
    let btnBack = UIButton()
    var activityView : ActivityView!
    var name : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupNavigationBar()

        self.setupActivityView()
        self.btnBack.isUserInteractionEnabled = false
        DispatchQueue.global(qos: .background).async {
            let document = PDFDocument(url: self.url!)
                DispatchQueue.main.async(execute: {
                    let pdfView = PDFView(frame: self.view.frame)
                    pdfView.document = document
                    pdfView.autoScales = true
                    pdfView.scaleFactor = pdfView.scaleFactorForSizeToFit
                    pdfView.minScaleFactor = pdfView.scaleFactorForSizeToFit
                    self.view.addSubview(pdfView)
                    
                    self.activityView.activityView.stopAnimating()
                    self.activityView.removeFromSuperview()
                    self.btnBack.isUserInteractionEnabled = true
                })
        }
    }
    
    func setupNavigationBar() {
        
        let width = self.view.frame.width - 60
        let viewLeftbar = UIView(frame: CGRect.init(x: 0, y: 0, width: width, height: 40))
        viewLeftbar.backgroundColor = .clear
        
        //let btnSideMenu = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 25, height: 40)
        btnBack.addTarget(self, action: #selector(btnBackClicked), for: .touchUpInside)
        
        lblTitle = UILabel(frame:  CGRect(x: 40, y: 0, width: width - 40, height: 40))
        lblTitle.text = "\(name!)"
        lblTitle.textColor = UIColor.white
        lblTitle.font = UIFont.boldSystemFont(ofSize: 17.0) //UIFont(name: "Merienda-Bold", size: 17.0)
        
        viewLeftbar.addSubview(btnBack)
        viewLeftbar.addSubview(lblTitle)
        
        let leftBarItem = UIBarButtonItem(customView: viewLeftbar)
        
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .black
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    @objc func btnBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }

    func setupActivityView() {
        activityView = (Bundle.main.loadNibNamed("ActivityView", owner: nil, options: nil)![0] as! ActivityView)
        activityView.activityView.startAnimating()
        self.activityView.frame = self.view.frame
        self.activityView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(self.activityView)
    }
}
