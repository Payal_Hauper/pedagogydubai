import UIKit
import SDWebImage

class GallerySlideShowVC: UIViewController {

    @IBOutlet weak var collectionViewSlideShow: UICollectionView!
    
    let btnBack = UIButton()
    var currentIndex = 0

    var arrAssets = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionViewSlideShow.register(UINib(nibName: "CustomCellSliderGallery", bundle: nil), forCellWithReuseIdentifier: "CustomCellSliderGallery")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.collectionViewSlideShow.scrollToItem(at: IndexPath(item: self.currentIndex, section: 0), at: .centeredHorizontally, animated: false)
        }
        self.setupNavigationBar()
    }
    
    func setupNavigationBar() {
        
        let width = self.view.frame.width - 60
        let viewLeftbar = UIView(frame: CGRect.init(x: 0, y: 0, width: width, height: 40))
        viewLeftbar.backgroundColor = .clear
        
        //let btnSideMenu = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 25, height: 40)
        btnBack.addTarget(self, action: #selector(btnBackClicked), for: .touchUpInside)
        
        let lblTitle = UILabel(frame:  CGRect(x: 40, y: 0, width: width - 40, height: 40))
        lblTitle.text = "Pedagogy Dubai"
        lblTitle.textColor = UIColor.white
        lblTitle.font = UIFont.boldSystemFont(ofSize: 17.0) // UIFont(name: "Merienda-Bold", size: 17.0)
        
        viewLeftbar.addSubview(btnBack)
        viewLeftbar.addSubview(lblTitle)
        
        let leftBarItem = UIBarButtonItem(customView: viewLeftbar)
        
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .black
        
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    @objc func btnBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }

}


extension GallerySlideShowVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrAssets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = self.collectionViewSlideShow.dequeueReusableCell(withReuseIdentifier: "CustomCellSliderGallery", for: indexPath) as! CustomCellSliderGallery
        cell.imgViewGallery.backgroundColor = UIColor.red
        let solidImage = UIColor.darkGray.image(CGSize(width: cell.imgViewGallery.frame.width, height: cell.imgViewGallery.frame.width))
        cell.imgViewGallery.sd_setImage(with: URL(string: self.arrAssets[indexPath.item]), placeholderImage: solidImage)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.collectionViewSlideShow.bounds.width, height: self.collectionViewSlideShow.bounds.height)
    }
}
