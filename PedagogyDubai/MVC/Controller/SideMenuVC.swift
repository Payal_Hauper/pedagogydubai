import UIKit

class SideMenuVC: UITableViewController {

    var arrSideMenuSection1 = ["Home","Event","Parent Community Connections"]
    
    @IBOutlet weak var imgProfilePic: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMail: UILabel!
    @IBOutlet weak var viewTop: UIView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    //MARK:- Methods
    func setup() {
        
        self.setupNavigationBar()
        
        self.view.layoutIfNeeded()
        self.navigationController?.navigationBar.isHidden  = true
        self.tableView.tableFooterView = UIView()
        
        let gredientLayer =  AppSingleton.sharedInstance().setGradientBackground(color1: UIColor.black, color2: UIColor.white)
        
        gredientLayer.frame = self.viewTop.bounds
        self.viewTop.layer.insertSublayer(gredientLayer, at:0)
        
        self.imgProfilePic.layer.cornerRadius = 32.5
        self.imgProfilePic.clipsToBounds = true
        
        let userDetails = UserDefaults.standard.value(forKey: "UserDetails") as! [String:Any]
        self.lblMail.text = userDetails["email"] as? String
        self.lblName.text = userDetails["child_name"] as? String
        self.downloadImage(from: URL(string: userDetails["image"]! as! String)!)
    }
    
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                self.imgProfilePic.image = UIImage(data: data)
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func setupNavigationBar() {
        
        self.navigationController?.navigationBar.barStyle = .default
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .black
        
        self.navigationController?.navigationBar.barStyle = .black
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSideMenuSection1.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCellSideMenu", for: indexPath) as! CustomCellSideMenu
        
        let bgColorView = UIView()
        bgColorView.backgroundColor = UIColor(red: 211.0/255.0, green: 211.0/255.0, blue: 211.0/255.0, alpha: 1.0)
        cell.selectedBackgroundView = bgColorView
        
        if AppSingleton.sharedInstance().selectedSideMenu.rawValue == indexPath.row {
            cell.backgroundColor = UIColor(red: 211.0/255.0, green: 211.0/255.0, blue: 211.0/255.0, alpha: 1.0)
            if indexPath.item == 2 {
                cell.imgIcon.image = UIImage(named:"sponsor_black")
            } else {
                cell.imgIcon.image = UIImage(named: "\(self.arrSideMenuSection1[indexPath.row].lowercased())_black")
            }
        } else {
            cell.backgroundColor = .clear
            if indexPath.item == 2 {
                cell.imgIcon.image = UIImage(named:"sponsor")
            } else {
                cell.imgIcon.image = UIImage(named: self.arrSideMenuSection1[indexPath.row].lowercased())
            }
        }
        cell.lblTitle.text = self.arrSideMenuSection1[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.row {
        case 0:
            NotificationCenter.default.post(name: Notification.Name("notificationOFChangeTab"), object: nil, userInfo: ["selectedTab": SideMenuOption.home])
        case 1:
            NotificationCenter.default.post(name: Notification.Name("notificationOFChangeTab"), object: nil, userInfo: ["selectedTab": SideMenuOption.event])
        default:
            NotificationCenter.default.post(name: Notification.Name("notificationOFChangeTab"), object: nil, userInfo: ["selectedTab": SideMenuOption.sponsor])
            print("Sponsor")
        }        
        self.dismiss(animated: true, completion: nil)
    }

}
