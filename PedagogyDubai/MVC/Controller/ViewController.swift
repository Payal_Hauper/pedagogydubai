
import UIKit

class ViewController: UIViewController {
    
    //MARK:- Outlets
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!
    @IBOutlet weak var imgViewYellowBrickRoadNursery: UIImageView!
    @IBOutlet weak var imgViewCrystalValleyBrickRoadNursery: UIImageView!
    @IBOutlet weak var btnYellowBrickRoadNursery: UIButton!
    @IBOutlet weak var btnCrystalValleyBrickRoadNursery: UIButton!
    
    //MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnYellowBrickRoadNursery.alpha = 0
        self.btnCrystalValleyBrickRoadNursery.alpha = 0
        self.imgViewYellowBrickRoadNursery.alpha = 0
        self.imgViewCrystalValleyBrickRoadNursery.alpha = 0
        self.imgIcon.alpha = 0
        self.lblTitle.alpha = 0
        
        self.navigationController?.navigationBar.isHidden  = true
        
        if !appOpen {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.setup()
            }
        } else {
            self.setupAnimatedView()
        }
    }
    
    //MARK:- Methods
    func setup() {
        
        appOpen = true
        UIView.animate(withDuration: 0.5, animations: {
            self.imgIcon.alpha = 1
            self.view.layoutIfNeeded()
        }) { (success) in
            
            if success {
                let time = DispatchTime.now() + 2
                DispatchQueue.main.asyncAfter(deadline: time, execute: {
                    
                    UIView.animate(withDuration: 0.2, animations: {
                        self.imgIcon.alpha = 0
                    }, completion: { (sucess) in
                        self.setupAnimatedView()
                    })
                })
            }
        }
    }
    
    
    func setupAnimatedView() {
        
        if UserDefaults.standard.bool(forKey: "UserLoginStatus") {
            print("User Already login")
            
            let transition = CATransition()
            transition.duration = 0.3
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.type = CATransitionType.fade
            self.navigationController!.view.layer.add(transition, forKey: nil)
            let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(homeVC, animated: false)
        } else {
            print("Go to login screen")
            UIView.animate(withDuration: 0.5, animations: {
                self.btnYellowBrickRoadNursery.alpha = 1
                self.btnCrystalValleyBrickRoadNursery.alpha = 1
                self.imgViewYellowBrickRoadNursery.alpha = 1
                self.imgViewCrystalValleyBrickRoadNursery.alpha = 1
                self.lblTitle.alpha = 1
            })
        }
        
        
    }
    
    
    //MARK:- Actions
    @IBAction func btnActions(_ sender: UIButton) {
        
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        
        if sender == btnYellowBrickRoadNursery {
            loginVC.loginType = .yellowBrickRoadNursery
            UserDefaults.standard.set(0, forKey: "NurseryType")
        } else {
            loginVC.loginType = .crystalValleyBrickRoadNursery
            UserDefaults.standard.set(1, forKey: "NurseryType")
        }
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    
}


