import UIKit
import SideMenu

protocol ProtocolDismissSideMenu : class {
    func dismissSideMenu()
}

class SideMenuContainerVC: UIViewController {

    @IBOutlet weak var btnLogout: UIButton!
    
    weak var delegateSideMenu : ProtocolDismissSideMenu?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func btnLogoutClicked(_ sender: UIButton) {
        
        AppSingleton.sharedInstance().simpleAlertviewWithTwoButtonCompletion(viewController: self, message: "Are you sure want to logout?", positiveActionButtonTitle: "Yes", nagativeActionButtonTitle: "No") { (success) in

            if success {
                self.delegateSideMenu?.dismissSideMenu()
            } else {
                print("cancel logout")
            }
        }
    }
}
