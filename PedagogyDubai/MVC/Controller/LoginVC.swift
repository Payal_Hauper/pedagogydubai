import UIKit
import SwiftyJSON
import Alamofire

class LoginVC: UIViewController {

    //MARK:-Outlets
    @IBOutlet weak var imgViewYellowBrickRoadNursery: UIImageView!
    @IBOutlet weak var imgViewCrystalValleyBrickRoadNursery: UIImageView!
    @IBOutlet weak var imgViewBG: UIImageView!
    @IBOutlet weak var viewLogin: UIView!
    @IBOutlet weak var viewEmail: UIView!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnGo: UIButton!
    
    var activityView : ActivityView!
    
    var loginType : EnumLoginType = .crystalValleyBrickRoadNursery
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }
    
    //MARK:- Methods
    
    func setup() {
        
        self.view.layoutIfNeeded()
        
        self.viewEmail.layer.cornerRadius = self.viewEmail.frame.height / 2
        self.viewPassword.layer.cornerRadius = self.viewPassword.frame.height / 2
        self.btnGo.layer.cornerRadius = self.btnGo.frame.height / 2
        
        self.viewLogin.alpha = 0
        
        //set img view frames
        let height  = (self.imgViewBG.frame.height * 0.12)
        let width = (height / 0.35)
        let x = (self.imgViewBG.center.x - (width / 2))
        
        self.setupTextfield()
        self.setupLoginView()

        if loginType == .yellowBrickRoadNursery {
            self.imgViewYellowBrickRoadNursery.alpha = 1
            self.imgViewCrystalValleyBrickRoadNursery.alpha = 0
            
            let yOfImgViewYellowBrick = (((self.imgViewBG.frame.height) * 0.612) / 2)
            let yPosition = ((yOfImgViewYellowBrick) - (height / 2))
            self.imgViewYellowBrickRoadNursery.frame = CGRect(x: x, y: yPosition, width: width, height: height)
            
            UIView.animate(withDuration: 0.5, animations: {
                
//                let y = (((self.imgViewBG.frame.height) * 0.5) / 2)
//                let yPosition = ((y) - (height / 2))
                let yPosition = self.viewLogin.frame.minY - height

                self.imgViewYellowBrickRoadNursery.frame = CGRect(x: x, y: yPosition, width: width, height: height)
                self.view.layoutIfNeeded()

            }) { (success) in
                self.viewLogin.alpha = 1
            }

        } else {
            self.imgViewYellowBrickRoadNursery.alpha = 0
            self.imgViewCrystalValleyBrickRoadNursery.alpha = 1
            
            let yOfImgViewCrystalValley = (((self.imgViewBG.frame.height) * 0.872) / 2)
            let yPosition1 = ((yOfImgViewCrystalValley) - (height / 2))
            self.imgViewCrystalValleyBrickRoadNursery.frame = CGRect(x: x, y: yPosition1, width: width, height: height)
            
            
            UIView.animate(withDuration: 0.5, animations: {
                
                //let y = (((self.imgViewBG.frame.height) * 0.5) / 2)
                //let yPosition1 = ((y) - (height / 2))
                let yPosition1 = self.viewLogin.frame.minY - height
                
                self.imgViewCrystalValleyBrickRoadNursery.frame = CGRect(x: x, y: yPosition1, width: width, height: height)
                self.view.layoutIfNeeded()

            }) { (success) in
                self.viewLogin.alpha = 1
            }
            
        }
        
    }
    
    func setupActivityView() {
        activityView = (Bundle.main.loadNibNamed("ActivityView", owner: nil, options: nil)![0] as! ActivityView)
        activityView.activityView.startAnimating()
        self.activityView.frame = self.view.frame
        self.activityView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(self.activityView)
    }
    
    func setupTextfield() {
        
        self.txtEmail.leftViewMode = .always
        let imageViewEmail = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let imgEmail = UIImage(named: "email")
        imageViewEmail.image = imgEmail
        
        let viewEmail: UIView = UIView(frame: CGRect(x: 20, y: 0, width: 30, height: 20))
        viewEmail.addSubview(imageViewEmail)
        self.txtEmail.leftView = viewEmail
        
        
        self.txtPassword.leftViewMode = .always
        let imageViewPassword = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let imgPassword = UIImage(named: "password")
        imageViewPassword.image = imgPassword
        
        let viewPassword: UIView = UIView(frame: CGRect(x: 20, y: 0, width: 30, height: 20))
        viewPassword.addSubview(imageViewPassword)
        self.txtPassword.leftView = viewPassword
        
        //set place holder text color
        txtEmail.attributedPlaceholder = NSAttributedString(string: "Email Address",
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(0.6)])
        
        txtPassword.attributedPlaceholder = NSAttributedString(string: "Password",
                                                               attributes: [NSAttributedString.Key.foregroundColor: UIColor.black.withAlphaComponent(0.6)])
    }
    
    func setupLoginView() {
        
        viewEmail.backgroundColor = UIColor.white
        viewEmail.layer.shadowColor = UIColor.lightGray.cgColor
        viewEmail.layer.shadowOpacity = 1
        viewEmail.layer.shadowOffset = CGSize.zero
        viewEmail.layer.shadowRadius = 3

        viewPassword.backgroundColor = UIColor.white
        viewPassword.layer.shadowColor = UIColor.lightGray.cgColor
        viewPassword.layer.shadowOpacity = 1
        viewPassword.layer.shadowOffset = CGSize.zero
        viewPassword.layer.shadowRadius = 3
    }
    
    func callLoginApi() {
        
        self.view.endEditing(true)

        self.setupActivityView()

        let parameters: [String:String] = ["email" : txtEmail.text!, "password" : txtPassword.text!,"device_token" : UserDefaults.standard.object(forKey: "DeviceToken") as! String, "device_type" : "ios", "device_id" : deviceID]

        apiManager.apiCalling(urlString: ApiDetails.login.url, method: ApiDetails.login.method, param: parameters as [String : AnyObject], header: ApiDetails.login.header, encodingTye: JSONEncoding.default) { (code, response, error) in
            
            print(response)
            
            self.activityView.activityView.stopAnimating()
            self.activityView.removeFromSuperview()
            
            guard error == nil else {
                AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: error!.localizedDescription, actionButtonTitle: "Ok")
                return
            }
            
            let dictData = response.dictionaryObject as! [String:AnyObject]

            if dictData["status"] as! Int == 1 {
                
                let emailID = dictData["data"]!["email"]! as! String
                let childName = dictData["data"]!["child_name"]! as! String
                let image = dictData["data"]!["image"]! as! String
                let id = dictData["data"]!["id"]! as! Int
                
                let userDetails = ["email":emailID,"id":id,"child_name":childName,"image":image] as [String : Any]
                UserDefaults.standard.set(userDetails, forKey: "UserDetails")
                UserDefaults.standard.set(true, forKey: "UserLoginStatus")
                UserDefaults.standard.set("\(id)", forKey: "UserID")

                let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(homeVC, animated: true)
            } else {
                let message = dictData["message"]! as! String
                AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: message, actionButtonTitle: "Ok")
                return
            }
        }
    }

    
    //MARK:- Actions
 
    @IBAction func btnGoClicked(_ sender: UIButton) {
        
//        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
//        self.navigationController?.pushViewController(homeVC, animated: true)
//        return
        
        if txtEmail.text?.count == 0 {
            AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: "Email should not be blank.", actionButtonTitle: "Ok")
        } else if txtPassword.text?.count == 0 {
            AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: "Password should not be blank.", actionButtonTitle: "Ok")
        } else if !AppSingleton.sharedInstance().validateEmail(enteredEmail: txtEmail.text!) {
            AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: "Please enter valid email address.", actionButtonTitle: "Ok")
        }
        else {
            
            if !Connectivity.isConnectedToInternet {
                AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: "No Inernet connection", actionButtonTitle: "Ok")
                return
            }
            self.callLoginApi()
        }
        
    }
    
}

extension LoginVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == self.txtEmail {
            self.txtPassword.becomeFirstResponder()
        } else {
            self.view.endEditing(true)
        }
        return false
    }
    
}
