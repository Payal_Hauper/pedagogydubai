//
//  PDFReaderVC.swift
//  PedagogyDubai
//
//  Created by === on 22/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import UIKit
import PDFKit

class PDFReaderVC: UIViewController {
    
    @IBOutlet weak var pdfView: PDFView!
    var lblTitle = UILabel()
    let btnBack = UIButton()
    
    var urlPDF : URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setupPDFViewer()
    }

    func setupPDFViewer() {
        
//        let pdfView = PDFView(frame: self.view.bounds)
        //pdfView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        self.view.addSubview(pdfView)
        
        // Fit content in PDFView.
        pdfView.autoScales = true
        
        if let document = PDFDocument(url: urlPDF!) {
            pdfView.document = document
        }
    }
    
    func setupNavigationBar() {
        
        let width = self.view.frame.width - 60
        let viewLeftbar = UIView(frame: CGRect.init(x: 0, y: 0, width: width, height: 40))
        viewLeftbar.backgroundColor = .clear
        
        //let btnSideMenu = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 25, height: 40)
        btnBack.addTarget(self, action: #selector(btnBackClicked), for: .touchUpInside)
        
        lblTitle = UILabel(frame:  CGRect(x: 40, y: 0, width: width - 40, height: 40))
        lblTitle.text = "Pedagogy Dubai"
        lblTitle.textColor = UIColor.white
        lblTitle.font = UIFont.boldSystemFont(ofSize: 17.0)
        
        viewLeftbar.addSubview(btnBack)
        viewLeftbar.addSubview(lblTitle)
        
        let leftBarItem = UIBarButtonItem(customView: viewLeftbar)
        
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .black
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    @objc func btnBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }

    
}
