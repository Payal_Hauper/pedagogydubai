import UIKit
import SwiftyJSON
import Alamofire
import SDWebImage
import AVKit

class FeedDetailsVC: UITableViewController {
    
    var lblTitle = UILabel()
    let btnBack = UIButton()
    var arrFeedDetails = [FeedDetailsDM]()
   // var arrFeedDetailsType3 = [FeedDetailsDMType3]()

    var activityView : ActivityView!
    var currentIndex = 0
    
    var heightForHeader : CGFloat = 0
    
    var apiType : Int?

    var eventDetails : EventDM?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.estimatedRowHeight = 200
        
        self.setupNavigationBar()
        self.tableView.tableFooterView =  UIView()
        let headerview = UIView()
        self.tableView.tableHeaderView = headerview
        
        switch AppSingleton.sharedInstance().selectedSideMenu {
        case .home:
            self.callFeedDetailsApi(type: apiType!)
        case .event:
            print("Event Type")
            
            if let eventData = eventDetails {
                self.lblTitle.text = eventData.name
                
                self.tableView.reloadData()
            }
        case .sponsor:
            print("sponsor Type")
        }
    }
    
    
    func setupNavigationBar() {
        
        let width = self.view.frame.width - 60
        let viewLeftbar = UIView(frame: CGRect.init(x: 0, y: 0, width: width, height: 40))
        viewLeftbar.backgroundColor = .clear
        
        //let btnSideMenu = UIButton(type: .custom)
        btnBack.setImage(UIImage(named: "back"), for: .normal)
        btnBack.frame = CGRect(x: 0, y: 0, width: 25, height: 40)
        btnBack.addTarget(self, action: #selector(btnBackClicked), for: .touchUpInside)
        
        lblTitle = UILabel(frame:  CGRect(x: 40, y: 0, width: width - 40, height: 40))
        lblTitle.text = ""
        lblTitle.textColor = UIColor.white
        lblTitle.font = UIFont.boldSystemFont(ofSize: 17.0) //UIFont(name: "Merienda-Bold", size: 17.0)
        
        viewLeftbar.addSubview(btnBack)
        viewLeftbar.addSubview(lblTitle)
        
        let leftBarItem = UIBarButtonItem(customView: viewLeftbar)
        
        self.navigationItem.setLeftBarButton(leftBarItem, animated: true)
        self.navigationController?.isNavigationBarHidden = false
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .black
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationController?.navigationBar.barStyle = .black
    }
    
    @objc func btnBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }

    func setupActivityView() {
        activityView = (Bundle.main.loadNibNamed("ActivityView", owner: nil, options: nil)![0] as! ActivityView)
        activityView.activityView.startAnimating()
        self.activityView.frame = self.view.bounds
        self.activityView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        self.view.addSubview(self.activityView)
    }
    
    //MARK:- Dashboard Api
    func callFeedDetailsApi(type: Int) {
        
        self.setupActivityView()
        
        let strURL = ApiDetails.feedDetails.url + "\(UserDefaults.standard.value(forKey: "UserID") ?? 0)/single_feed/" + "\(currentIndex)?type=\(type)"
        
        apiManager.apiCalling(urlString: strURL, method: ApiDetails.feedDetails.method, param: nil, header: ApiDetails.feedDetails.header, encodingTye: JSONEncoding.default) { (code, response, error) in
            
            self.activityView.activityView.stopAnimating()
            self.activityView.removeFromSuperview()
            
            guard error == nil else {
                AppSingleton.sharedInstance().simpleAlertview(viewController: self, message: error!.localizedDescription, actionButtonTitle: "Ok")
                return
            }
            
            guard let dictData = response.dictionary, (dictData["data"]?.arrayValue.count != 0 || dictData["data"]?.dictionary != nil) else {
                AppSingleton.sharedInstance().simpleAlertviewWithCompletion(viewController: self, message: "Failed to load Data.", actionButtonTitle: "Ok", completion: { (success) in
                    
                    if success {
                        self.navigationController?.popViewController(animated: true)
                    }
                })
                return
            }
            let arrResponseData = dictData["data"]!.dictionaryObject
            
            let arrFeedDetailsData = FeedDetailsDM.feedDetailsData(jsonArray: [arrResponseData!])
            for i in arrFeedDetailsData {
                self.arrFeedDetails.append(i)
            }
            
            print(self.arrFeedDetails)
            
            if self.arrFeedDetails.count > 0 {
                self.heightForHeader = 16
                self.tableView.reloadData()
                self.lblTitle.text =  self.arrFeedDetails.first!.title
            }
        }
    }
    
    func isVideo() -> Bool {
        
        if self.arrFeedDetails.count > 0 {
            
            if self.arrFeedDetails.first!.assets.count > 0  && self.arrFeedDetails.first!.assets.first!.contains(".mp4") {
                    return true
            } else {
                return false
            }
        } else {
            return false
        }
    }
    
    func pushToGallerySlideShowScreen(assets : [String],index : Int) {
        let gallerySlideShowVC = self.storyboard?.instantiateViewController(withIdentifier: "GallerySlideShowVC") as! GallerySlideShowVC
        gallerySlideShowVC.arrAssets = assets
        gallerySlideShowVC.currentIndex = index
        self.navigationController?.pushViewController(gallerySlideShowVC, animated: true)
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppSingleton.sharedInstance().selectedSideMenu == .home ? self.arrFeedDetails.count == 0 ? 0 : 5 + self.arrFeedDetails.first!.milestone_points.count + 1 : 5
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailImageTableViewCell", for: indexPath) as? DetailImageTableViewCell else {
                return UITableViewCell()
            }
            switch AppSingleton.sharedInstance().selectedSideMenu {
            case .home:
                
                if let assetaName = self.arrFeedDetails.first!.assets.first {
                    cell.setUp(urlString: assetaName, isShowPlayBtn: self.isVideo())
                }
            case .event:
                print("Event Type")
                // print("event data :- \(eventDetails)")
                
                if let eventData = eventDetails {
                    cell.setUp(urlString: eventData.image, isShowPlayBtn: false)
                }
            
            case .sponsor:
                return UITableViewCell()
            }
            
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailImageCollectionViewTableViewCell", for: indexPath) as? DetailImageCollectionViewTableViewCell else {
                return UITableViewCell()
            }
            cell.colllectionViewImages.reloadData()
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailDurationTableViewCell", for: indexPath) as? DetailDurationTableViewCell else {
                return UITableViewCell()
            }
            if let eventData = eventDetails {
                cell.setUp(dateString: "Date: \(eventData.start_date) - \(eventData.end_date)")
            }
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailTimeTableViewCell", for: indexPath) as? DetailTimeTableViewCell else {
                return UITableViewCell()
            }
            if let eventData = eventDetails {
                cell.setUp(timeAgoString: eventData.time_ago)
            } else {
                cell.setUp(timeAgoString: self.arrFeedDetails.first!.time_ago)
            }
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailDescriptionTableViewCell", for: indexPath) as? DetailDescriptionTableViewCell else {
                return UITableViewCell()
            }
            if let eventData = eventDetails {
                cell.setUp(descString: eventData.description)
            } else {
               cell.setUp(descString: self.arrFeedDetails.first!.description)
            }
            return cell
            
        default:
            if indexPath.row < self.arrFeedDetails.first!.milestone_points.count + 5 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "DetailMilestoneTableViewCell") as? DetailMilestoneTableViewCell else {
                    return UITableViewCell()
                }
                cell.setUp(data: self.arrFeedDetails.first!.milestone_points[indexPath.row - 5])
                
                cell.viewUnderline.isHidden = self.arrFeedDetails.first!.milestone_points.count - 1 == indexPath.row - 5
                
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "MilestoneTableViewCell", for: indexPath) as? MilestoneTableViewCell else {
                    return UITableViewCell()
                }
                cell.setUp(data: self.arrFeedDetails.first!)
                return cell
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch AppSingleton.sharedInstance().selectedSideMenu {
        case .home:
            print("Home Type")
            
            if indexPath.row == 2 || indexPath.row == 3 {
                return 0
            }
            
            if self.arrFeedDetails.count == 0 {
                return 0
            }
            
            if indexPath.row == 0 {
                
                if let _ = self.arrFeedDetails.first!.assets.first {
                    return (self.tableView.frame.width * 11) / 16
                }
                return 0
            }
            
            if indexPath.row == 1 {
                if self.isVideo() {
                    return 0
                } else {
                    
                    if self.arrFeedDetails.first!.assets.count == 0 {
                        return 0
                    }
                    let cellWidth = (self.tableView.bounds.width - 24) / 4
                    
                    var lines = abs(self.arrFeedDetails.first!.assets.count / 4)
                    
                    if (self.arrFeedDetails.first!.assets.count % 4) == 0 {
                        return (CGFloat(lines) * cellWidth) + CGFloat((8 * (lines - 1)))
                    } else {
                        lines = lines + 1
                        return (CGFloat(lines) * cellWidth) + CGFloat((8 * (lines - 1)))
                    }
                }
            }
            
            if indexPath.row > 4 {
                if self.arrFeedDetails.first!.milestone_points.count == 0 {
                    if self.arrFeedDetails.first!.comment == "" {
                        return 0
                    }
                    return UITableView.automaticDimension //40
                }
                if indexPath.row < 5 + self.arrFeedDetails.first!.milestone_points.count {
                    return UITableView.automaticDimension
                }
                else {
                    if self.arrFeedDetails.first!.comment == "" {
                        return 0
                    }
                    return UITableView.automaticDimension //40
                }
            }
            
        case .event:
            print("Event Type")
            
            if indexPath.row == 1 || indexPath.row == 3 {
                return 0
            }
            
//            if indexPath.row == 2 || indexPath.row == 3 {
//                return 0
//            }

            
        case .sponsor:
            print("sponsor Type")
        }

        self.view.layoutIfNeeded()
        let height = (self.tableView.frame.width * 9) / 16 // self.tableView.frame.height
        
        if indexPath.row == 0 {
            
            if let _ = self.eventDetails?.image {
                return height
            }
            return 0
        } else {
            
            if indexPath.row == 4 && self.arrFeedDetails.first?.description == "" {
                return 0
            }
            return UITableView.automaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch AppSingleton.sharedInstance().selectedSideMenu {
        case .home:
            print("Home Type")
            if indexPath.row == 0 {
                if self.isVideo() {
                    let videoURL = URL(string: self.arrFeedDetails.first!.assets.first!)
                    let player = AVPlayer(url: videoURL! as URL)
                    let playerViewController = AVPlayerViewController()
                    playerViewController.player = player
                    self.present(playerViewController, animated: true) {
                        playerViewController.player!.play()
                    }
                } else {
                    self.pushToGallerySlideShowScreen(assets: self.arrFeedDetails.first!.assets, index: indexPath.row)
                }
            }
        case .event:
            self.pushToGallerySlideShowScreen(assets: [self.eventDetails!.image], index: 0)
        case .sponsor:
            print("sponsor Type")
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0 //self.heightForHeader
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 0))
        headerView.backgroundColor = UIColor.red
        return headerView
    }

}

extension FeedDetailsVC : UICollectionViewDataSource,UICollectionViewDelegate , UICollectionViewDelegateFlowLayout{
       
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if self.arrFeedDetails.count != 0 {
            return self.arrFeedDetails.first!.assets.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCellFeedDetails", for: indexPath) as! CustomCellFeedDetails
        let solidImage = UIColor.darkGray.image(CGSize(width: cell.imgViewGallery.frame.width, height: cell.imgViewGallery.frame.width))
        cell.imgViewGallery.sd_setImage(with: URL(string: self.arrFeedDetails.first!.assets[indexPath.item]), placeholderImage: solidImage)
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let gallerySlideShowVC = self.storyboard?.instantiateViewController(withIdentifier: "GallerySlideShowVC") as! GallerySlideShowVC
        gallerySlideShowVC.arrAssets = self.arrFeedDetails.first!.assets
        gallerySlideShowVC.currentIndex = indexPath.item
        self.navigationController?.pushViewController(gallerySlideShowVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth = (collectionView.bounds.width - 24) / 4
        let size = CGSize(width: cellWidth, height: cellWidth)
        return size
    }
    
}
