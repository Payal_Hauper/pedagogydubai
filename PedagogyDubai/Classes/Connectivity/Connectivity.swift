//
//  Connectivity.swift
//  PedagogyDubai
//
//  Created by === on 16/08/19.
//  Copyright © 2019 ===. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class var isConnectedToInternet:Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
}
