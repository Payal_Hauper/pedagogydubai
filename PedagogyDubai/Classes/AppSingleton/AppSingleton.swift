import UIKit

typealias simpleAlertWithCompletion = (_ boolSuccess : Bool) -> Void

class AppSingleton: NSObject {

    static var instance : AppSingleton!
    
    class func sharedInstance() -> AppSingleton {
        
        self.instance = (self.instance ?? AppSingleton())
        return self.instance
    }
    
    var selectedSideMenu : SideMenuOption = .home
    
    func setGradientBackground(color1 : UIColor, color2 : UIColor) -> CAGradientLayer {
        let colorTop =  color1.cgColor
        let colorBottom = color2.cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.startPoint = CGPoint(x: 0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.5)

        gradientLayer.locations = [0.0, 1.0]
        
        return gradientLayer
    }
    
    //MARK:- Email Validation
    func validateEmail(enteredEmail:String) -> Bool {
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: enteredEmail)
    }
    
    //MARK:- Alert
    func simpleAlertviewWithCompletion(viewController : UIViewController,message : String,actionButtonTitle : String , completion: @escaping simpleAlertWithCompletion) {
        
        let alertContrller = UIAlertController(title: appName, message: message, preferredStyle: .alert)
        let actionButton = UIAlertAction(title: actionButtonTitle, style: .default) { (action) in
            completion(true)
        }
        alertContrller.addAction(actionButton)
        viewController.present(alertContrller, animated: true, completion: nil)
    }
    
    func simpleAlertviewWithTwoButtonCompletion(viewController : UIViewController,message : String,positiveActionButtonTitle : String,nagativeActionButtonTitle : String , completion: @escaping simpleAlertWithCompletion) {
        
        let alertContrller = UIAlertController(title: appName, message: message, preferredStyle: .alert)
        let actionPositiveButton = UIAlertAction(title: positiveActionButtonTitle, style: .default) { (action) in
            alertContrller.dismiss(animated: true, completion: nil)
            completion(true)
        }
        let actionNagativeButton = UIAlertAction(title: nagativeActionButtonTitle, style: .default) { (action) in
            alertContrller.dismiss(animated: true, completion: nil)
            completion(false)
        }

        alertContrller.addAction(actionNagativeButton)
        alertContrller.addAction(actionPositiveButton)
        viewController.present(alertContrller, animated: true, completion: nil)
    }
    
    func simpleAlertview(viewController : UIViewController,message : String,actionButtonTitle : String) {
        
        let alertContrller = UIAlertController(title: appName, message: message, preferredStyle: .alert)
        let actionButton = UIAlertAction(title: actionButtonTitle, style: .default, handler: nil)
        alertContrller.addAction(actionButton)
        viewController.present(alertContrller, animated: true, completion: nil)
    }
    
}

extension UIColor {
    func image(_ size: CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { rendererContext in
            self.setFill()
            rendererContext.fill(CGRect(origin: .zero, size: size))
        }
    }
}
