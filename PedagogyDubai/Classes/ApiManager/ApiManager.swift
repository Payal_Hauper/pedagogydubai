import UIKit
import Alamofire
import SwiftyJSON

typealias apiCompletionHandler = (_ code : Int,_ response : JSON,_ error : Error?) -> Void


class ApiManager: NSObject {

    static var instance : ApiManager!
    
    class func sharedInstance() -> ApiManager {
        self.instance = (self.instance ?? ApiManager())
        return self.instance
    }
    
    func apiCalling(urlString : String, method : String, param : [String:AnyObject]?,header : [String:String]?,encodingTye: JSONEncoding,completion: @escaping apiCompletionHandler) {
        
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
            let url = URL(string: urlString)
            print(urlString)
            guard url != nil else {
                print("URL is nil")
                return
            }
            if method == GETREQ {
                Alamofire.request(url!, method: .get, parameters: param, encoding: encodingTye, headers: header).responseJSON { (responseData) in
                    print(responseData)
                    let isSuccess = JSON(responseData.result.isSuccess)
                    if isSuccess.boolValue {
                        let object =  JSON(responseData.result.value)
                        completion(1,object,nil)
                    } else {
                        let error = responseData.result.error
                        completion(0,nil,error)
                    }
                }
            } else {
                Alamofire.request(url!, method: .post, parameters: param, encoding: encodingTye, headers: header).responseJSON { (responseData) in
                    print(responseData)
                    let isSuccess = JSON(responseData.result.isSuccess)
                    if isSuccess.boolValue {
                        let object =  JSON(responseData.result.value)
                        completion(1,object,nil)
                    } else {
                        let error = responseData.result.error
                        completion(0,nil,error)
                    }
                }
            }
        }
    }
    
    /*
    func apiCallingWithBody(urlString : String, method : String, param : [String:AnyObject]?,header : [String:String]?, body : String,encodingTye: JSONEncoding,completion: @escaping apiCompletionHandler) {

        let url = URL(string: urlString)
        guard url != nil else {
            print("URL is nil")
            return
        }
        print(urlString)
        let json = body
        let jsonData = json.data(using: .utf8, allowLossyConversion: false)!
        var request = URLRequest(url: url!)
        request.httpMethod = HTTPMethod.post.rawValue
        request.setValue(header!["Content-Type"], forHTTPHeaderField: "Content-Type")
        request.httpBody = jsonData
        Alamofire.request(request).responseJSON { (responseData) in
            print(responseData)
            print("")
        }
    }
    */

}
