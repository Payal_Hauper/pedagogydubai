import Foundation

enum ApiDetails {
    case login
    case dashboard
    case feedDetails
    case event
    case sponsor
    
    var url : String {
        
        switch self {
            
        case .login:
            return baseURL + "auth/login"
        case .dashboard:
            return baseURL + "users/" //+ "6" + "/feed/"
        case . feedDetails:
            return baseURL + "users/" //+ "6" + "/single_feed/"
        case .event:
            return baseURL + "events/"
        case .sponsor:
            return baseURL + "sponsors"
        }
    }
    
    var method : String {
        
        switch self {
        case .login:
            return POSTREQ
        case .dashboard:
            return GETREQ
        case .feedDetails:
            return GETREQ
        case .event:
            return GETREQ
        case .sponsor:
            return GETREQ
        }
    }
    
    var header : [String:String] {
        switch self {
        case .login:
            return ["Content-Type": "application/json; charset=UTF-8"]
        case .dashboard:
            return ["Content-Type": "application/json"]
        case .feedDetails:
            return ["Content-Type": "application/json"]
        case .event:
            return ["Content-Type": "application/json"]
        case .sponsor:
            return ["Content-Type": "application/json"]
        }
    }

}
